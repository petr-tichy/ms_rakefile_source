require 'hpricot'
require 'open-uri'
require 'pp'
require 'tmpdir'
require 'tempfile'
require 'pathname'
require 'whenever'

ROOT                      = Pathname.new('/mnt/ms')
TOOLS_ROOT                = ROOT + 'tools'
RAKE_HOME                 = TOOLS_ROOT + 'rakefile_source'
GEMFILE_HOME              = ROOT
CHIPMUNK_ROOT             = TOOLS_ROOT + 'ms_projects'
CLTOOL_ROOT               = TOOLS_ROOT + 'cltool'
CLOVER_ROOT               = TOOLS_ROOT + 'clover'
SCRIPT_ROOT               = TOOLS_ROOT + 'script'
PROJECTS_TEMPLATE_ROOT    = CHIPMUNK_ROOT + 'template'
PROJECTS_TEMPLATE         = PROJECTS_TEMPLATE_ROOT + 'pa'
PROJECTS_ROOT             = CHIPMUNK_ROOT + 'projects'
DEPLOYED_PROJECTS_DIR     = ROOT + 'deployed_projects'
PROVISIONING_DIR          = ROOT + 'deployments'
CLOVER_BUCKET             = "gdc-ms-cz-msf-engine"

RUBY_TOOLS = ['rforce', 'ms_archiver', 'ms_downloader', 'infra', 'gooddata-ruby', 'salesforce', 'user_hierarchies', 'es', 'sfdc_tests', 'gd', 'variable_uploader', 'ms_utils', 'psql_logger','project_validator']
namespace :infra do

  desc "Update self"
  task :update_self do
    update_git RAKE_HOME
  end

  desc "Teardown of infrastructure"
  task :teardown do
    FileUtils::cd TOOLS_ROOT do
      RUBY_TOOLS.each do |tool|
        FileUtils::rm_rf tool
      end
      FileUtils::rm_rf 'Gemfile'
    end
    
    FileUtils::rm_rf CHIPMUNK_ROOT
    FileUtils::rm_rf CLTOOL_ROOT
    
    FileUtils::rm_rf CLOVER_ROOT
    FileUtils::rm_rf SCRIPT_ROOT

    FileUtils::cd GEMFILE_HOME do
      FileUtils::rm_f "Gemfile"
      FileUtils::rm_f "Gemfile.lock"
    end
  end

  task :setup_directory do
    FileUtils::mkdir_p TOOLS_ROOT
  end

  desc "Initial setup of infrastructure"
  task :init => [
    :teardown,
    :setup_directory,
    :clone_ruby_tools,
    :download_latest_cl,
    :download_latest_clover,
    :clone_ms_projects,
    :copy_script_tools,
    :setup_bundler] do
  end

  task :update_ms_projects do
    update_git CHIPMUNK_ROOT
  end

  desc "update tools"
  task :update => [
    :update_ms_projects,
    :update_ruby_tools,
    :copy_script_tools,
    :update_bundler] do
  end

  task :clone_ms_projects do
    FileUtils::cd TOOLS_ROOT do
      system_and_throw "git clone git@github.com:gooddata/ms_projects.git"
    end
  end

  task :copy_script_tools do
    FileUtils::rm_rf SCRIPT_ROOT
    FileUtils::cp_r CHIPMUNK_ROOT + 'tools/script', SCRIPT_ROOT
    
    FileUtils::cd SCRIPT_ROOT do
      system_and_throw "chmod o+x sf-download-validation.sh"
      system_and_throw "chmod o+x sf-report-validation.sh"
      system_and_throw "chmod o+x sf-user-sync.sh"
    end
  end

  task :setup_clover do
    FileUtils::mkdir_p CLOVER_ROOT
  end

  desc "install latest clover version from s3"
  task :download_latest_clover => [:setup_clover] do
#     FileUtils::cd CLOVER_ROOT do
#       doc = Hpricot::XML(open "http://s3.amazonaws.com/#{CLOVER_BUCKET}/")
#       clover_versions = doc.search("ListBucketResult Contents Key").map {|el| el.innerHTML}
#       distro_file = clover_versions.sort.last
# 
#       FileUtils::rm_f distro_file
#       system_and_throw "wget http://s3.amazonaws.com/#{CLOVER_BUCKET}/#{distro_file}"
#       system_and_throw "tar -xvf #{distro_file}"
#       FileUtils::rm_f distro_file
#       local_dir = distro_file.sub('.tar.gz', '')
# 
#       FileUtils::rm_f "current"
#       system_and_throw "ln -s #{local_dir} current"
#     end
  end

  task :update_ruby_tools do
    # Grab ruby tools
    FileUtils::cd TOOLS_ROOT do
      RUBY_TOOLS.each do |dir|
        update_git dir
      end
    end
  end

  task :clone_ruby_tools do
    # Grab ruby tools
    FileUtils::cd TOOLS_ROOT do
      system_and_throw "git clone git://github.com/gooddata/gooddata-ruby.git"
      system_and_throw "git clone git://github.com/fluke777/salesforce.git"
      system_and_throw "git clone git://github.com/fluke777/user_hierarchies.git"
      system_and_throw "git clone git://github.com/fluke777/es.git"
      system_and_throw "git clone git@github.com:gooddata/sfdc_tests.git"
      system_and_throw "git clone git://github.com/fluke777/gd.git"
      system_and_throw "git clone git://github.com/fluke777/variable_uploader.git"
      system_and_throw "git clone git://github.com/fluke777/infra.git"
      system_and_throw "git clone git://github.com/fluke777/ms_downloader.git"
      system_and_throw "git clone git://github.com/Tereci/ms_utils.git"
      system_and_throw "git clone git://github.com/fluke777/rforce.git"
      system_and_throw "git clone git://github.com/fluke777/ms_archiver.git"
      system_and_throw "git clone git://github.com/Tereci/psql_logger.git"
      system_and_throw "git clone git://github.com/adriantoman/project_validator.git"
    end
  end

  task :setup_cl do
    FileUtils::mkdir_p CLTOOL_ROOT
  end

  desc "download latest version of CL tool"
  task :download_latest_cl => [:setup_cl] do |t, args|
    version = get_latest_cl_version
    FileUtils::cd CLTOOL_ROOT do
      system_and_throw "curl --remote-name --location https://github.com/downloads/gooddata/GoodData-CL/gooddata-cli-#{version}.zip
      system_and_throw "unzip gooddata-cli-#{version}.zip"
      FileUtils::rm_f "gooddata-cli-#{version}.zip"
      FileUtils::rm_f "current"
      system_and_throw "ln -s gooddata-cli-#{version} current"
    end
  end

  desc "download CL tool, version could be specified as 1.2.50"
  task :download_cl_version, [:version] => [:setup_cl] do |t, args|
    version = args[:version]
    puts version
    FileUtils::cd CLTOOL_ROOT do
      system_and_throw "curl --remote-name --location https://github.com/downloads/gooddata/GoodData-CL/gooddata-cli-#{version}.zip
      system_and_throw "unzip gooddata-cli-#{version}.zip"
      FileUtils::rm_f "gooddata-cli-#{version}.zip"
    end
  end

  task :setup_bundler do
    cd TOOLS_ROOT do
      system_and_throw "git clone git@github.com:gooddata/ms_gemfile.git Gemfile"
    end
    cd GEMFILE_HOME do
      system_and_throw "ln -s #{TOOLS_ROOT + 'Gemfile/Gemfile'} Gemfile"
      system_and_throw "ln -s #{TOOLS_ROOT + 'Gemfile/Gemfile.lock'} Gemfile.lock"
      system_and_throw "bundle install --path bundled_gems --binstubs"
    end
  end

  task :update_bundler do
    cd TOOLS_ROOT do
      FileUtils::cd "Gemfile" do
        system_and_throw "git reset --hard"
      end
      update_git "Gemfile"
      
    end
    cd GEMFILE_HOME do
      system_and_throw "bundle install --path bundled_gems --binstubs"
    end
  end

end

namespace :project do

  desc "Create new project"
  task :create, :customer, :project do |t, args|
    customer = args[:customer]
    project_name = args[:project]
    fail "Provide customer name" if customer.nil? || customer.empty?
    fail "Provide project name" if project_name.nil? || project_name.empty?
    project_dir = PROJECTS_ROOT + customer + project_name
    fail "Project #{project_dir} already exists" if project_dir.exist?
    puts "Creating project #{customer}/#{project_name}"
    puts
    FileUtils::mkdir_p project_dir
    FileUtils::cd project_dir do
      dirs = ['data/estore-in', 'data/estore-out', 'data/gooddata', 'data/source', 'data/transform', 'descriptor', 'downloader', 'estore', 'graph', 'log', 'model', 'script']
      dirs.each do |dir|
        puts "Creating directory #{dir}"
        FileUtils::mkdir_p dir
        FileUtils::cd dir do
          puts "Creating file .gitignore in #{dir}"
          FileUtils::touch '.gitignore'
        end
      end
    end

    ['app', 'schedule'].each do |file|
      puts "Copying file #{file}"
      FileUtils::cp PROJECTS_TEMPLATE + file, project_dir
    end

    puts "Generating params.json"
    params = {
      "CUSTOMER"  => customer,
      "PROJECT"   => project_name,
      "TIMEZONE"  => "",
      "ES_NAME"   => project_name,
      "LOGIN"     => "",
      "PASSWORD"  => ""
    }
    File.open(project_dir + 'params.json', 'w') do |f|
      f.write JSON.pretty_generate(params)
    end
  end

  desc "deploy a project"
  task :deploy, :customer, :project do |t, args|
    customer  = args[:customer]
    project   = args[:project]
    fail "You have to specify customer" if customer.nil? || customer.empty?
    fail "You have to specify project" if project.nil? || project.empty?
    fail "Seems like project #{customer}/#{project} does not exist" unless File.exist?(PROJECTS_ROOT + customer + project)

    Dir.mktmpdir(nil,"/mnt/tmp") {|dir|
      dir_pathname = Pathname.new(dir)
      system_and_throw "git clone --no-hardlinks #{TOOLS_ROOT + 'ms_projects'} #{dir}"
      FileUtils::cd dir_pathname do
        system_and_throw "git remote rm origin"
        system_and_throw "git remote add origin git@github.com:gooddata/ms_projects.git"
        update_git
      end
      
      project_source_path = dir_pathname + 'projects' + args[:customer] + args[:project]
      project_dest_path = DEPLOYED_PROJECTS_DIR + args[:customer] + args[:project]
      
      if File.exist?(project_dest_path)
        # deploy in place
        puts "deploy in place"
        FileUtils::cd project_dest_path do
          list = (Dir.glob('*') - ["data", "log", "run_params.json", "workspace.prm", "downloader"])
          list.each {|dir| rm_rf dir}
        end
        Dir.glob(project_source_path + '**/*').each do |file|
          dest_file = file.sub(project_source_path, project_dest_path)
          if File.directory?(file)
            FileUtils::mkdir_p dest_file
          else
            FileUtils::cp(file, dest_file) unless File.exist?(dest_file)
          end
        end
      else
        # deploy fresh
        puts "deploy fresh"
        FileUtils::mkdir_p project_dest_path
        FileUtils::cp_r project_source_path, project_dest_path.dirname
      end
      
    }
  end

  desc "pack project"
  task :pack, :customer, :project do |t, args|
    fail "You have to specify customer" if args[:customer].nil? || args[:customer].empty?
    fail "You have to specify project" if args[:project].nil? || args[:project].empty?
    project_src = DEPLOYED_PROJECTS_DIR + args[:customer] + args[:project]
    FileUtils::cd DEPLOYED_PROJECTS_DIR do
      system_and_throw "zip -r #{args[:customer]}-#{args[:project]}-#{Date.today.to_s}.zip #{project_src}"
    end
  end

  desc "schedule"
  task :schedule_all, :force do |t, args|
    # $stderr = File.open('/dev/null', 'w')
      begin
        f = Tempfile.open('ms_rakefile_schedule')
        f << "set :job_template, \"bash -cl ':job'\";"
        f << "job_type(:run_project, 'cd :project_path && /mnt/ms/bin/infra run >/dev/null 2>&1;');"
        f << "job_type(:run_old_project, 'cd :project_path && ./run.sh >/dev/null 2>&1;');"
        f << "job_type(:run_project_provisioning, 'cd :project_path && ./run.sh >/dev/null 2>&1;');"
        FileUtils::cd DEPLOYED_PROJECTS_DIR do
          workspace_list  = Dir.glob("*/*/run.sh").map {|file| Pathname.new(file).expand_path.dirname}
          app_list        = Dir.glob("*/*/app").map {|file| Pathname.new(file).expand_path.dirname}
          list = workspace_list + app_list
          app_list.each do |dir|
            FileUtils::cd dir do
              unless File.exist?('schedule')
                puts "Project in #{dir} does not have schedule" 
                next
              end
              File.readlines('schedule').each do |line|
                f << "#{line.chomp} do run_project('foo', :project_path => '#{dir}'); end;"
              end
            end
          end
          workspace_list.each do |dir|
            FileUtils::cd dir do
              unless File.exist?('schedule')
                puts "Project in #{dir} does not have schedule" 
                next
              end
              schedule = File.read('schedule').chomp
              f << "#{schedule} do run_old_project('foo', :project_path => '#{dir}'); end;"
            end
          end
        end
        FileUtils::cd PROVISIONING_DIR do
          provisioning_list  = Dir.glob("*/*/run.sh").map {|file| Pathname.new(file).expand_path.dirname}
          provisioning_list.each do |dir|
            FileUtils::cd dir do
              unless File.exist?('schedule')
                puts "Provisionig in #{dir} does not have schedule" 
                next
              end
              schedule = File.read('schedule').chomp
              f << "#{schedule} do run_project_provisioning('foo', :project_path => '#{dir}'); end;"
            end
          end
        end
        f.flush
        options = {}
        options[:write] = true unless args[:force].nil?
        options[:file] = f.path
        # options[:identifier] = dir
        Whenever::CommandLine.execute(options)
      rescue SystemExit => e
        puts e.message
      rescue => e
        puts e.message
      ensure
        f.close
        f.unlink
      end
    end
  end

def system_and_throw(command)
  puts "Running external command \"#{command}\""
  system command
  fail "external command failed" if $?.exitstatus != 0
  puts "********"
  puts
end

def get_latest_cl_version
  doc = Hpricot(open('https://github.com/gooddata/GoodData-CL/downloads'))
  elements = doc.search("#uploaded_downloads h4 a")
  loads = elements.map {|el| "https://github.com#{el.attributes['href']}"}.find_all {|load| load.include?(".zip")}
  loads.first.match("gooddata-cli-(.*).zip")[1]
end

def update_git(dir=nil)
  if dir
    cd dir do
      system_and_throw "git pull --rebase origin master"
    end
  else
    system_and_throw "git pull --rebase origin master"
  end
end

def install_gem(gem)
  system_and_throw "gem install #{gem} --no-rdoc --no-ri"
end
