#!/bin/bash
set -e

gem install rake --no-rdoc --no-ri
gem install hpricot  --no-rdoc --no-ri
gem install bundler  --no-rdoc --no-ri
gem install whenever  --no-rdoc --no-ri
gem install pry  --no-rdoc --no-ri
gem install rainbow  --no-rdoc --no-ri
gem update --system 1.8.24

mkdir -p /mnt/ms/tools
cd /mnt/ms/tools

rm -rf rakefile_source
git clone git@github.com:gooddata/ms_rakefile_source.git rakefile_source

cd ..
rm -f Rakefile
ln -s tools/rakefile_source/Rakefile-sla Rakefile
rake sla:init